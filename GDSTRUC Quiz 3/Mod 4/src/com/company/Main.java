package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        ArrayQueue queue = new ArrayQueue(20);
        Random randomNum = new Random();

        int gameRound = 1;
        while (gameRound < 11)
        {
            System.out.println("-------- Queueing for Game "+ gameRound + " --------");
            System.out.println("Game requires 5 players to play! Queueing...");
            gameQueue(queue, randomNum);
            while (queue.size() < 5)
            {
                gameQueue(queue, randomNum);
            }
            queue.printingQueue();
            System.out.println("\nRemoving Players from Queue...");
            System.out.println("Removed Players: ");
            for (int i = 0; i < 5 ; i++)
            {
                System.out.println(queue.removeFromQueue());
            }
            System.out.println("-------- Game "+ gameRound + " has ended --------");
            gameRound++;
            System.out.println("Press Enter...");
            new java.util.Scanner(System.in).nextLine();
        }
    }

    public static void gameQueue(ArrayQueue queue, Random randomNum)
    {
        int randomizedNum = 1 + randomNum.nextInt(7);
        System.out.println( "Players added to Queue: " + randomizedNum);

        for (int i = 0 ; i < randomizedNum ; i ++)
        {
            queue.addToQueue(new Player("Random Player"));
        }
    }
    

}
