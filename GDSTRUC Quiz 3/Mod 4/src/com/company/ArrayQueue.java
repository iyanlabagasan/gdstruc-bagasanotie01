package com.company;

import java.util.NoSuchElementException;

public class ArrayQueue {

    private Player[] queue;
    private int front;
    private int back;

    public ArrayQueue(int capacity)
    {
        queue = new Player[capacity];

    }

    public void addToQueue(Player player)
    {
        if (back == queue.length)
        {
            Player[] newArray = new Player[queue.length *2];
            System.arraycopy(queue, 0, newArray, 0, queue.length);
            queue = newArray;
        }

        queue[back] = player;
        back++;
    }

    public Player removeFromQueue()
    {
        if (size()==0)
        {
            throw new NoSuchElementException();
        }
        Player removePlayer = queue[front];
        queue[front] = null;
        front++;

        if (size() ==  0)
        {
            front = 0 ;
            back = 0 ;
        }
        return removePlayer;
    }

    public int size()
    {
        return back - front;
    }
    public void printingQueue()
    {
        for (int i = front; i < back; i++)
        {
            System.out.println(queue[i]);
        }
    }
}
