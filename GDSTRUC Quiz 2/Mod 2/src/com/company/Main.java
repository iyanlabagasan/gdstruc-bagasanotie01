package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        List<Player> playerList = new ArrayList<>();


        Player ynla = new Player (1, "Ynla", 102);
        Player two = new Player (2, "TWO",99);
        Player three = new Player (3, "THREE", 101);
        Player four = new Player (4, "FOUR", 55);
        Player five = new Player (5, "FIVE", 105);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        //Add
        playerLinkedList.addToFront(ynla);
        playerLinkedList.addToFront(two);
        playerLinkedList.addToFront(three);
        playerLinkedList.addToFront(four);
        playerLinkedList.addToFront(five);

        //Print list and Count
        playerLinkedList.printList();
        playerLinkedList.elementCount();

        //Print with RemoveElement
        playerLinkedList.removeElement();
        playerLinkedList.printList();

        //Print Element Count
        playerLinkedList.elementCount();

        //Print True or False
        System.out.println("\n\nContains Element: ");
        System.out.println(playerLinkedList.elementContain(five));
        System.out.println(playerLinkedList.elementContain(four));
        System.out.println(playerLinkedList.elementContain(three));
        System.out.println(playerLinkedList.elementContain(two));
        System.out.println(playerLinkedList.elementContain(ynla));

        //Print Index of the ff. Elements
        System.out.println("\n\nElement Index: ");
        System.out.println(playerLinkedList.elementIndex(four));
        System.out.println(playerLinkedList.elementIndex(ynla));
        System.out.println(playerLinkedList.elementIndex(three));
        System.out.println(playerLinkedList.elementIndex(two));
    }




}
