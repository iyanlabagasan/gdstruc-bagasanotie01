package com.company;

public class PlayerLinkedList {
    private PlayerNode head;

    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void printList()
    {
        PlayerNode current = head;
        System.out.print("Head -> ");
        while (current != null) {
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    // Remove the First Element
    public void removeElement()
    {
        head = head.getNextPlayer();

    }

    // Show the size of the list
    public void elementCount()
    {
        PlayerNode current = head;
       int sizeCount = 0;
       while (current !=null){
           current = current.getNextPlayer(); sizeCount++;
       }
       System.out.println("Number of Elements: " + sizeCount);
    }

    //Contains Element
    public boolean elementContain(Player find)
    {
        PlayerNode current = head;
        while (current != null)
        {
            if (current.getPlayer() == find)
            {
                return true;
            }
            current = current.getNextPlayer();
        }
        return false;
    }

    //Index of Element
    public int elementIndex(Player find)
    {
        PlayerNode current = head;
        int index = 0;
        while (current.getPlayer() != find)
        {
            current = current.getNextPlayer();
            index++;
        }
        return index;
    }
}
