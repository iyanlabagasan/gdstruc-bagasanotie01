package com.company;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[10];

        numbers[0] = 35;
        numbers[1] = 69;
        numbers[2] = 1;
        numbers[3] = 10;
        numbers[4] = -50;
        numbers[5] = 320;
        numbers[6] = 63;
        numbers[7] = 58;
        numbers[8] = 26;
        numbers[9] = 13;

        // Print before Sorting
        System.out.println("Array Before Sorting: ");
        printArrayElements(numbers);

        // Print Bubble Sort
        bubbleSort(numbers);
        System.out.println("\n\n  Bubble Sort: ");
        printArrayElements(numbers);

        // Print Selection Sort
        selectionSort(numbers);
        System.out.println("\n\n Selection Sort: ");
        printArrayElements(numbers);

        // Print Modified Selection Sort
        modifiedselectionSort(numbers);
        System.out.println("\n\n  Modified Selection Sort: ");
        printArrayElements(numbers);


    }
    // Bubble Sort
    private static void bubbleSort(int[] arr)
    { for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        { for (int i = 0 ; i < lastSortedIndex; i++)
            { if (arr[i] < arr[i + 1])
                { int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp; } } }
    }


    // Selection Sort
    private static void selectionSort(int[] arr)
    { for (int lastSortedIndex = 0; lastSortedIndex < arr.length - 1 ; lastSortedIndex++)
        { int largestIndex = lastSortedIndex;
            for (int i = lastSortedIndex + 1; i < arr.length; i++)
            { if (arr[i] > arr[largestIndex])
                {
                    largestIndex = i;
                }}
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[largestIndex];
            arr[largestIndex]  = temp; }
    }


    // Modified Selection Sort
    private static void modifiedselectionSort(int[] arr)
    { for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0 ; lastSortedIndex--)
        { int smallestIndex = 0;
            for (int i = lastSortedIndex; i > 0; i--)
            { if (arr[i] < arr[smallestIndex])
            { smallestIndex = i; } }
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex]  = temp; }
    }


    private static void printArrayElements(int[] arr)
    { for (int i = 0; i < arr.length; i++)
        { System.out.println(arr[i]); }
    }
}

