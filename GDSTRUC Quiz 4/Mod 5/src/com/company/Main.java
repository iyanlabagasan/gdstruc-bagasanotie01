package com.company;

public class Main {

    public static void main(String[] args) {

        Player ploo = new Player("Plooful");
        Player wardell = new Player("TSM Wardell");
        Player deadlyJimmy = new Player("DeadlyJimmy");
        Player subroza = new Player("Subroza");
        Player annieDro = new Player("C9 Annie");

        SimpleHashtable hashtable = new SimpleHashtable();
        hashtable.put(ploo.getName(), ploo);
        hashtable.put(wardell.getName(), wardell);
        hashtable.put(deadlyJimmy.getName(), deadlyJimmy);
        hashtable.put(subroza.getName(), subroza);
        hashtable.put(annieDro.getName(), annieDro);

        hashtable.printHashtable();

        System.out.println(hashtable.get("Subroza"));

        System.out.println("\nRemoving Player: Subroza");
        System.out.print(hashtable.removePlayer("Subroza"));

        System.out.println("\n\nFinding Player: Subroza ");
        System.out.print(hashtable.get("Subroza"));
    }
}
