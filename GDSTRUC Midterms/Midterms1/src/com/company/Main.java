package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        CardStack deck = new CardStack();

        CardStack hand = new CardStack();

        CardStack discardPile = new CardStack();

        Random outcomes = new Random();

        for (int i = 0; i < 3; i++) {
            deck.push(new Card("King"));
            deck.push(new Card("Queen"));
            deck.push(new Card("Jack"));
            deck.push(new Card("Hearts"));
            deck.push(new Card("Diamonds"));
            deck.push(new Card("Ace"));
            deck.push(new Card("Spades"));
            deck.push(new Card("Clover"));
            deck.push(new Card("Zero"));
            deck.push(new Card("Joker"));
        }


        boolean deckStack = false;
        int round = 1;
        while (!deckStack)
        {
            System.out.println("Round: " + round);
            round++;

            System.out.println("_______________________________");
            turnOutcome(deck,hand,discardPile,outcomes);
            System.out.println("_______________________________");


            cardPrint(deck,hand,discardPile);
            System.out.println("_______________________________");


            if (deck.isEmpty())
            {
                deckStack = true;
                System.out.println("Deck is now empty!");
            }
        }

    }

    public static void turnOutcome(CardStack deck, CardStack hand, CardStack discardPile, Random outcomes)
    {
        int randomOutcome = 1 + outcomes.nextInt(3);
        //Draw from Deck

         if (randomOutcome == 1)
         {
             System.out.println("Draw from Deck! ");
             randomOutcome = 1 + outcomes.nextInt(5);

             int size = deck.size();
             if (randomOutcome > size)
             {
                randomOutcome = size;
             }

             for (int i = 0; i < randomOutcome; i++ )
             {
                 System.out.println("Drawn: " + deck.peek());
                 Card placeholder = deck.peek();
                 hand.push(placeholder);
                 deck.pop();
             }
         }
         //Discard
         else if (randomOutcome == 2)
         {
             System.out.println("Time to discard from Hand! ");
            if (hand.isEmpty())
            {
                System.out.println("Player Hand: Empty. No cards have been discarded.");
            }
            else {
                randomOutcome = 1 + outcomes.nextInt(5);

                int size = hand.size();
                if (randomOutcome > size)
                {
                    randomOutcome = size;
                }

                for (int i = 0; i < randomOutcome; i++)
                {
                    System.out.println("Discard: " + hand.peek());
                    Card placeholder = hand.peek();
                    discardPile.push(placeholder);
                    hand.pop();

                }
            }
         }
         //Draw from Discarded Pile
         else if (randomOutcome == 3)
         {
             System.out.println("Draw from Discarded Pile! ");
             if (discardPile.isEmpty())
             {
                 System.out.println("Discarded Pile: Empty. No cards have been drawn.");
             }
             else
                 {
                 randomOutcome = 1 + outcomes.nextInt(5);

                 int size = discardPile.size();
                 if (randomOutcome > size)
                 {
                     randomOutcome = size;
                 }

                 for (int i = 0; i < randomOutcome; i++)
                 {
                     System.out.println("Drawn from Discarded Pile:  " + discardPile.peek());
                     Card placeholder = discardPile.peek();
                     hand.push(placeholder);
                     discardPile.pop();
                 }
             }
         }
    }

    public static void cardPrint(CardStack deck, CardStack hand, CardStack discardPile)
    {
        // Deck Count
        System.out.print("Cards left in the deck: ");
        System.out.println(deck.size());

        // Player Hand
        int handCount = hand.size();
        if (handCount == 0)
        {
            System.out.println("Player Hand: Empty");
        }
        else {
            System.out.println("Cards in hand: ");
            hand.printStack();
        }

        // Discarded Pile Count

        int discardCount = discardPile.size();
        if (discardCount == 0)
        {
            System.out.println("Discarded Pile: Empty");
        }
        else {
            System.out.print("Discarded Pile: ");
            System.out.println(discardPile.size());
        }

    }
}
